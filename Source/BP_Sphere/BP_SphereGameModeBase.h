// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BP_SphereGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BP_SPHERE_API ABP_SphereGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
